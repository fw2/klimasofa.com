#!/bin/bash

case "$1" in
    u)
        php composer.phar info > var/logs/composer.$(date "+%Y-%m-%d-%H%M%S").info
        ;;
    update)
        php composer.phar info > var/logs/composer.$(date "+%Y-%m-%d-%H%M%S").info
        ;;
    upgrade)
        php composer.phar info > var/logs/composer.$(date "+%Y-%m-%d-%H%M%S").info
        ;;
    install)
        php composer.phar info > var/logs/composer.$(date "+%Y-%m-%d-%H%M%S").info
        ;;
    *)
        ;;
esac

php -d memory_limit=-1 -d max_execution_time=900 composer.phar $@

