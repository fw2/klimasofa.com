
jQuery(document).ready(function($) {

    accordion();
    toggleSearch();

    function accordion() {

        var accordion = $('.accordion');

        accordion.hide();

        var
            statusClosed = '<span class="status closed offscreen">Aufklappen: </span>',
            statusOpen = '<span class="status open offscreen">Zuklappen: </span>';

        accordion.each(function(index) {

            var thisAccordion = $(this);
            var thisItem = thisAccordion.find('.ce_text');

            thisItem.each(function(index) {

                var thisTitle = $(this).children('h2, h3, h4, h5, h6');
                var thisTextContainer = $(this).find('.text_container');
                var thisImageContainer = $(this).find('.image_container');

                $(this).addClass('accordion-item').addClass('closed');

                thisTitle.wrap('<div class="accordion-item-title"></div>');
                thisTitle.wrapInner('<a href="#" class="accordion-item-title-link closed" />');
                thisTitle.find('a').attr('aria-expanded', 'false').prepend(statusClosed);

                if (thisTextContainer.parent('.inside').length) {
                    thisTextContainer.unwrap();
                }
                thisTextContainer.hide();
                thisTextContainer.addClass('accordion-item-content').wrapInner('<div class="accordion-item-content-inner"></div>');

                thisImageContainer.prependTo(thisTextContainer);
            });

        });

        accordion.show();

        $( '.accordion-item-title-link' ).click(function( e ) {

            var
                thisLink    = $(this),
                thisItem    = thisLink.parents( '.accordion-item' ),
                thisContent = thisItem.find( '.accordion-item-content' ),
                thisStatus  = thisLink.find( '.status' );

            thisContent.slideToggle( 400 );

            if (thisStatus.hasClass( 'closed' )) {
                thisStatus.remove();
                thisLink
                    .addClass( 'open' )
                    .addClass( 'cursor-over')
                    .removeClass('closed')
                    .attr( 'aria-expanded','true' )
                    .prepend( statusOpen );
                thisItem
                    .addClass( 'open' )
                    .removeClass( 'closed' );
                thisContent
                    .addClass('open')
                    .show();
                e.preventDefault();
            }
            else {
                thisStatus.remove();
                thisLink
                    .addClass( 'closed' )
                    .addClass( 'cursor-over')
                    .removeClass( 'open' )
                    .attr( 'aria-expanded','false' )
                    .prepend( statusClosed );
                thisItem
                    .removeClass( 'open' )
                    .addClass( 'closed' );
                thisContent
                    .hide();
                e.preventDefault();
            }
            e.preventDefault();
        });

        $('.accordion-item-title-link').mouseleave(function( e ) {
            var thisLink = $(this);
            thisLink.removeClass( 'cursor-over' );
        });

        $('.accordion-item-title-link').focusin(function( e ) {
            var thisLink = $(this);
            thisLink.addClass( 'cursor-over' );
        });

    }

    function toggleSearch() {

        $('.search-button').click(function( e ) {
            $('.header-search').addClass('open');
            $('.search-input').removeClass('hidden');
            $('.search-cover').removeClass('hidden');
        });

        $('.search-cover').click(function( e ) {
            $('.header-search').removeClass('open');
            $('.search-input').addClass('hidden');
            $('.search-cover').addClass('hidden');
        });

    }

});
